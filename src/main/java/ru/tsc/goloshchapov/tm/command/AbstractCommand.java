package ru.tsc.goloshchapov.tm.command;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.service.IServiceLocator;
import ru.tsc.goloshchapov.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        if (!name().isEmpty()) result += name() + " ";
        if (arg() != null && !arg().isEmpty()) result += "(" + arg() + ") ";
        if (!description().isEmpty()) result += description();
        return result;
    }

}
