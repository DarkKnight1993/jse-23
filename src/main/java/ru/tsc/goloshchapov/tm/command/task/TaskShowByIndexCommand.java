package ru.tsc.goloshchapov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.command.AbstractTaskCommand;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {
    @NotNull
    @Override
    public String name() {
        return "task-show-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK INFO BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
