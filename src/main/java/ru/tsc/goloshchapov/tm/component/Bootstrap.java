package ru.tsc.goloshchapov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.repository.*;
import ru.tsc.goloshchapov.tm.api.service.*;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;
import ru.tsc.goloshchapov.tm.command.auth.*;
import ru.tsc.goloshchapov.tm.command.project.*;
import ru.tsc.goloshchapov.tm.command.projecttask.ProjectTaskBindByIdCommand;
import ru.tsc.goloshchapov.tm.command.projecttask.ProjectTaskShowByIdCommand;
import ru.tsc.goloshchapov.tm.command.projecttask.ProjectTaskUnbindByIdCommand;
import ru.tsc.goloshchapov.tm.command.system.*;
import ru.tsc.goloshchapov.tm.command.task.*;
import ru.tsc.goloshchapov.tm.command.user.UserByLoginLockCommand;
import ru.tsc.goloshchapov.tm.command.user.UserByLoginRemoveCommand;
import ru.tsc.goloshchapov.tm.command.user.UserByLoginUnlockCommand;
import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.exception.entity.CommandNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.repository.*;
import ru.tsc.goloshchapov.tm.service.*;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectTaskService, projectRepository);

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, authRepository);

    @Getter
    @NotNull
    private final ILogService logService = new LogService();

    private void initData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Gamma", "-", Status.COMPLETED));
        projectService.add(userService.findByLogin("admin").getId(), new Project("Project Alpha", "-"));
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Beta", "-", Status.IN_PROGRESS));
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Delta", "-", Status.COMPLETED));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Gamma", "-", Status.COMPLETED));
        taskService.add(userService.findByLogin("admin").getId(), new Task("Task Alpha", "-"));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Beta", "-", Status.IN_PROGRESS));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Delta", "-", Status.COMPLETED));
    }

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new ProjectTaskBindByIdCommand());
        registry(new ProjectTaskUnbindByIdCommand());
        registry(new ProjectTaskShowByIdCommand());

        registry(new AboutShowCommand());
        registry(new ArgumentsListShowCommand());
        registry(new CommandsListShowCommand());
        registry(new ExitCommand());
        registry(new HelpShowCommand());
        registry(new InfoShowCommand());
        registry(new VersionShowCommand());

        registry(new AuthChangePasswordCommand());
        registry(new AuthLoginCommand());
        registry(new AuthLogoutCommand());
        registry(new AuthRegistryCommand());
        registry(new AuthUpdateProfileCommand());
        registry(new AuthViewProfileCommand());

        registry(new UserByLoginLockCommand());
        registry(new UserByLoginRemoveCommand());
        registry(new UserByLoginUnlockCommand());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable String[] args) {
        System.out.println("\n** WELCOME TO TASK MANAGER **");
        initData();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private void process() {
        logService.debug("Test environment");
        @Nullable String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("\nENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
            } catch (final Exception exception) {
                logService.error(exception);
            }
        }
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new CommandNotFoundException(args[0]);
        command.execute();
        return true;
    }

    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new CommandNotFoundException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

}
