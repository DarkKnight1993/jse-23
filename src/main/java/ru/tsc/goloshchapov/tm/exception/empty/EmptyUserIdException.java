package ru.tsc.goloshchapov.tm.exception.empty;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Exception! User Id is empty!");
    }

}
