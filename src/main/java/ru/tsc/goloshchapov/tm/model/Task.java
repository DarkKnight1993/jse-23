package ru.tsc.goloshchapov.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.entity.IWBS;
import ru.tsc.goloshchapov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Task extends AbstractOwnerEntity implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId = null;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    private Date created = new Date();

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public Task(@NotNull String name, @NotNull String description, @NotNull Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return id + ": " + name;
    }

}
