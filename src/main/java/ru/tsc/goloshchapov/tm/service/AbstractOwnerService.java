package ru.tsc.goloshchapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.IOwnerRepository;
import ru.tsc.goloshchapov.tm.api.IOwnerService;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyIndexException;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyUserIdException;
import ru.tsc.goloshchapov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.goloshchapov.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    @NotNull
    protected IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(@NotNull IOwnerRepository<E> ownerRepository) {
        super(ownerRepository);
        this.ownerRepository = ownerRepository;
    }

    @Override
    public void add(@Nullable String userId, @Nullable E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new EntityNotFoundException();
        ownerRepository.add(userId, entity);
    }

    @Override
    public void remove(@Nullable String userId, @Nullable E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new EntityNotFoundException();
        ownerRepository.remove(userId, entity);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.existsById(userId, id);
    }

    @Override
    public boolean existsByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.existsByIndex(userId, index);
    }

    @Nullable
    @Override
    public List<E> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ownerRepository.findAll(userId);
    }

    @Nullable
    @Override
    public List<E> findAll(@Nullable String userId, @Nullable Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return ownerRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public E findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @Nullable
    @Override
    public E findByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    public E removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.removeById(userId, id);
    }

    @Nullable
    @Override
    public E removeByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.removeByIndex(userId, index);
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        ownerRepository.clear(userId);
    }

}
