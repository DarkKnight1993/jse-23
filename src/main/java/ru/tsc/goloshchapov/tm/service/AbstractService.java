package ru.tsc.goloshchapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.IRepository;
import ru.tsc.goloshchapov.tm.api.IService;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyIndexException;
import ru.tsc.goloshchapov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.goloshchapov.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected IRepository<E> repository;

    public AbstractService(@NotNull IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(@Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.add(entity);
    }

    @Override
    public void remove(@Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public boolean existsByIndex(@Nullable Integer index) {
        if (index == null || index < 0) return false;
        return repository.existsByIndex(index);
    }

    @Nullable
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<E> findAll(@Nullable Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public E findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public E findByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(index);
    }

    @Nullable
    @Override
    public E removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public E removeByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(index);
    }

    @Override
    public void clear() {
        repository.clear();
    }
}
