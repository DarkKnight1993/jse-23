package ru.tsc.goloshchapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.repository.IAuthRepository;
import ru.tsc.goloshchapov.tm.api.service.IAuthService;
import ru.tsc.goloshchapov.tm.api.service.IUserService;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyLoginException;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyPasswordException;
import ru.tsc.goloshchapov.tm.exception.user.AccessDeniedException;
import ru.tsc.goloshchapov.tm.model.User;
import ru.tsc.goloshchapov.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IAuthRepository authRepository;

    public AuthService(@NotNull IUserService userService, @NotNull IAuthRepository authRepository) {
        this.userService = userService;
        this.authRepository = authRepository;
    }

    @Nullable
    @Override
    public User getUser() {
        @Nullable final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        @Nullable final String userId = authRepository.getUserId();
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return authRepository.isUserIdExists();
    }

    @Override
    public void logout() {
        authRepository.clearUserId();
    }

    @Override
    public void login(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        authRepository.setUserId(user.getId());
    }

    @Override
    public void registry(@Nullable String login, @Nullable String password, @Nullable String email) {
        userService.create(login, password, email);
    }

    @Override
    public void updateProfile(@Nullable String userId, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName) {
        userService.updateUser(userId, firstName, middleName, lastName);
    }

    @Override
    public void changePassword(@Nullable String userId, @Nullable String password) {
        userService.setPassword(userId, password);
    }

    @Override
    public void checkRoles(@Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        @NotNull final Role role = user.getRole();
        for (final Role r : roles) {
            if (r.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
